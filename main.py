import xml.etree.ElementTree as et
import sys
import os
from time import sleep

#Čtení z xml souboru
tree = et.parse('story.xml')
root = tree.getroot()

#Povolené vstupy hráče a jejich definice
def title_screen_actions():
    action = input("> ")
    
    match action.lower():
        case "hrat":
            main_game()
        case "napoveda":
            help_game()
        case "odejit":
            title_screen()
        case "ukoncit":
            sys.exit()
        case _:
            while action.lower() not in ["hrat", "napoveda", "odejit", "ukoncit"]:
                print("Neplatný vstup, prosím zadejte platný vstup")
                action = input("> ")

                match action.lower():
                    case "hrat":
                        main_game()
                    case "napoveda":
                        help_game()
                    case "odejit":
                        title_screen()
                    case "ukoncit":
                        sys.exit()

#Grafické prostředí hlavního menu
def title_screen():
    os.system('cls')
    print("*************")
    print("  LongNight  ")
    print("-------------")
    print("-Hrát")
    print("-Nápověda")
    print("-Ukončit")
    print("-------------")
    print("Copyright Daniel Chochola 2024")
    title_screen_actions()

#Nápověda pro hráče
def help_game():
    os.system('cls')
    print("*************")
    print("  LongNight  ")
    print("-------------")
    print("-Pro zvolení možnosti napište příkaz bez použití diakritiky např. pro zvolení možnosti Hrát napište 'hrat', atd.")
    print("-Na otázky odpovídejte příslušným písmenkem")
    print("-Pro navracení na hlavní obrazovku použijte příkaz 'odejit'")
    print("-------------")
    print("Copyright Daniel Chochola 2024")
    title_screen_actions()

#Hlavní funkce hry
def main_game():
    #Vyčištění terminálu pro lepší zážitek
    os.system('cls')

    #Čtení uvítecí zprávy z xml dokumentu
    welcome = root[0].text
    new_welcome = welcome.strip()
    #Animace vypisování textu
    for i in new_welcome:
        print(i, end="")
        sys.stdout.flush()
        sleep(0.1)
    sleep(0.3)
    os.system('cls')

    vypravec1 = root[1].text
    new_vypravec1 = vypravec1.strip()
    for i in new_vypravec1:
        print(i, end="")
        sys.stdout.flush()
        sleep(0.1)
    sleep(0.3)

    print("\n")

    vypravec2 = root[2].text
    new_vypravec2 = vypravec2.strip()
    for i in new_vypravec2:
        print(i, end="")
        sys.stdout.flush()
        sleep(0.1)
    sleep(0.3)

    print("\n")

    print("Kolemjdoucí: ")
    kolemjdouci1 = root[3].text
    new_kolemjdouci1 = kolemjdouci1.strip()
    for i in new_kolemjdouci1:
        print(i, end="")
        sys.stdout.flush()
        sleep(0.1)
    sleep(0.3)
    print("\n")

    #Nabídka dialogových možností
    print("a) ", root[4].text.strip(), end="")
    print("\n")
    print("b) ", root[5].text.strip(), end="")
    print("\n")
    answer1 = input("> ")

    #Zvolení dialogové možnosti a její logické vyhodnocení
    ##První možný konec hry
    if answer1.lower() == "a":
        os.system('cls')
        konec1 = root[6].text
        new_konec1 = konec1.strip()
        for i in new_konec1:
            print(i, end="")
            sys.stdout.flush()
            sleep(0.1)
        sleep(0.3)

        #Konec hry
        os.system('cls')
        print("\nKONEC HRY")
        sleep(3)
        os.system('cls')
        title_screen()
        title_screen_actions()

    #Pokračování hry
    elif answer1.lower() == "b":
        os.system('cls')

        print("Kolemjdoucí: ")
        kolemjdouci2 = root[7].text
        new_kolemjdouci2 = kolemjdouci2.strip()
        for i in new_kolemjdouci2:
            print(i, end="")
            sys.stdout.flush()
            sleep(0.1)
        sleep(0.3)

        print("\n")

        print("Jan: ")
        jan1 = root[8].text
        new_jan1 = jan1.strip()
        for i in new_jan1:
            print(i, end="")
            sys.stdout.flush()
            sleep(0.1)
        sleep(0.3)

        print("\n")

        print("Kolemjdoucí: ")
        kolemjdouci3 = root[9].text
        new_kolemjdouci3 = kolemjdouci3.strip()
        for i in new_kolemjdouci3:
            print(i, end="")
            sys.stdout.flush()
            sleep(0.1)
        sleep(0.3)

        print("\n")

        print("Jan: ")
        jan2 = root[10].text
        new_jan4 = jan2.strip()
        for i in new_jan4:
            print(i, end="")
            sys.stdout.flush()
            sleep(0.1)
        sleep(0.3)

        print("\n")

        print("Kolemjdoucí: ")
        kolemjdouci4 = root[11].text
        new_kolemjdouci4 = kolemjdouci4.strip()
        for i in new_kolemjdouci4:
            print(i, end="")
            sys.stdout.flush()
            sleep(0.1)
        sleep(0.3)

        print("\n")

        #Druhý výběr z dialogových možností
        print("a) ", root[12].text.strip(), end="")
        print("\n")
        print("b) ", root[13].text.strip(), end="")
        print("\n")
        answer2 = input("> ")

        #Druhý možný konec hry
        if answer2 == "a":
            os.system('cls')

            print("Doktor: ")
            doktor1 = root[14].text
            new_doktor1 = doktor1.strip()
            for i in new_doktor1:
                print(i, end="")
                sys.stdout.flush()
                sleep(0.1)
            sleep(0.3)

            print("\n")

            vypravec3 = root[15].text
            new_vypravec3 = vypravec3.strip()
            for i in new_vypravec3:
                print(i, end="")
                sys.stdout.flush()
                sleep(0.1)
            sleep(0.3)

            print("\n")

            print("Jan: ")
            jan3 = root[16].text
            new_jan3 = jan3.strip()
            for i in new_jan3:
                print(i, end="")
                sys.stdout.flush()
                sleep(0.1)
            sleep(0.3)

            print("\n")

            print("Doktor: ")
            doktor2 = root[17].text
            new_doktor2 = doktor2.strip()
            for i in new_doktor2:
                print(i, end="")
                sys.stdout.flush()
                sleep(0.1)
            sleep(0.3)

            print("\n")

            vypravec4 = root[18].text
            new_vypravec4 = vypravec4.strip()
            for i in new_vypravec4:
                print(i, end="")
                sys.stdout.flush()
                sleep(0.1)
            sleep(0.3)

            print("\n")
            
            print("Doktor: ")
            doktor3 = root[19].text
            new_doktor3 = doktor3.strip()
            for i in new_doktor3:
                print(i, end="")
                sys.stdout.flush()
                sleep(0.1)
            sleep(0.3)

            print("\n")

            print("Doktor: ")
            doktor4 = root[20].text
            new_doktor4 = doktor4.strip()
            for i in new_doktor4:
                print(i, end="")
                sys.stdout.flush()
                sleep(0.1)
            sleep(0.3)

            print("\n")

            print("Jan: ")
            jan4 = root[21].text
            new_jan4 = jan4.strip()
            for i in new_jan4:
                print(i, end="")
                sys.stdout.flush()
                sleep(0.1)
            sleep(0.3)

            print("\n")

            konec21 = root[22].text
            new_konec21 = konec21.strip()
            for i in new_konec21:
                print(i, end="")
                sys.stdout.flush()
                sleep(0.1)
            sleep(0.3)

            print("\n")

            konec22 = root[23].text
            new_konec22 = konec22.strip()
            for i in new_konec22:
                print(i, end="")
                sys.stdout.flush()
                sleep(0.1)
            sleep(0.3)

            print("\n")

            konec23 = root[24].text
            new_konec23 = konec23.strip()
            for i in new_konec23:
                print(i, end="")
                sys.stdout.flush()
                sleep(0.1)
            sleep(0.3)

            #Konec hry
            os.system('cls')
            print("\nKONEC HRY")
            sleep(3)
            os.system('cls')
            title_screen()
            title_screen_actions()

        #Třetí možný konec hry
        elif answer2 == "b":
            os.system('cls')

            print("Doktor: ")
            doktor5 = root[25].text
            new_doktor5 = doktor5.strip()
            for i in new_doktor5:
                print(i, end="")
                sys.stdout.flush()
                sleep(0.1)
            sleep(0.3)

            print("\n")

            print("Jan: ")
            jan5 = root[26].text
            new_jan5 = jan5.strip()
            for i in new_jan5:
                print(i, end="")
                sys.stdout.flush()
                sleep(0.1)
            sleep(0.3)

            print("\n")
            
            print("Doktor: ")
            doktor6 = root[27].text
            new_doktor6 = doktor6.strip()
            for i in new_doktor6:
                print(i, end="")
                sys.stdout.flush()
                sleep(0.1)
            sleep(0.3)

            print("\n")

            vypravec5 = root[28].text
            new_vypravec5 = vypravec5.strip()
            for i in new_vypravec5:
                print(i, end="")
                sys.stdout.flush()
                sleep(0.1)
            sleep(0.3)

            print("\n")

            vypravec6 = root[29].text
            new_vypravec6 = vypravec6.strip()
            for i in new_vypravec6:
                print(i, end="")
                sys.stdout.flush()
                sleep(0.1)
            sleep(0.3)
            
            print("\n")

            print("Doktor: ")
            doktor7 = root[30].text
            new_doktor7 = doktor7.strip()
            for i in new_doktor7:
                print(i, end="")
                sys.stdout.flush()
                sleep(0.1)
            sleep(0.3)

            print("\n")

            print("Jan: ")
            jan6 = root[31].text
            new_jan6 = jan6.strip()
            for i in new_jan6:
                print(i, end="")
                sys.stdout.flush()
                sleep(0.1)
            sleep(0.3)

            print("\n")

            print("Doktor: ")
            doktor8 = root[32].text
            new_doktor8 = doktor8.strip()
            for i in new_doktor8:
                print(i, end="")
                sys.stdout.flush()
                sleep(0.1)
            sleep(0.3)

            print("\n")

            konec31 = root[33].text
            new_konec31 = konec31.strip()
            for i in new_konec31:
                print(i, end="")
                sys.stdout.flush()
                sleep(0.1)
            sleep(0.3)

            print("\n")

            konec32 = root[34].text
            new_konec32 = konec32.strip()
            for i in new_konec32:
                print(i, end="")
                sys.stdout.flush()
                sleep(0.1)
            sleep(0.3)

            print("\n")

            konec33 = root[35].text
            new_konec33 = konec33.strip()
            for i in new_konec33:
                print(i, end="")
                sys.stdout.flush()
                sleep(0.1)
            sleep(0.3)

            print("\n")

            konec34 = root[36].text
            new_konec34 = konec34.strip()
            for i in new_konec34:
                print(i, end="")
                sys.stdout.flush()
                sleep(0.1)
            sleep(0.3)

            #Konec hry
            os.system('cls')
            print("\nKONEC HRY")
            sleep(3)
            os.system('cls')
            title_screen()
            title_screen_actions()
title_screen()